import telebot
from telebot import types
from telebot.types import InputMediaPhoto
from core import generator
from core import func
import random
import os

bot = telebot.TeleBot(read_file('token.ini'))

flag = 0
flag2 = 0
top_text = None
bottom_text = None
filename = None
game = 0


def create_own_meme_buttons(message):
    markup = types.ReplyKeyboardMarkup(row_width=2, resize_keyboard=True)
    btn1 = types.KeyboardButton('Задать верхний текст')
    btn2 = types.KeyboardButton('Задать нижний текст')
    btn3 = types.KeyboardButton('Задать изображение')
    btn4 = types.KeyboardButton('Сгенерировать')
    btn5 = types.KeyboardButton('Посмотреть, что осталось сделать')
    btn6 = types.KeyboardButton('Рандомная картинка')
    markup.add(btn1, btn2, btn3, btn4, btn5, btn6)
    send_message = 'Выбери:' + '\n' + '-верхний текст' + '\n' + '-нижний текст' + '\n' + '-картинку' + '\n' + 'жми "Сгенерировать"'
    bot.send_message(message.chat.id, send_message, parse_mode='html', reply_markup=markup)


@bot.message_handler(commands=['start'])
def start(message):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    btn1 = types.KeyboardButton('Да, давай начнем!')
    markup.add(btn1)
    name_surname = f"<b>Привет {message.from_user.first_name}</b>!"
    start_message = 'Я бот-мемодел, ' + '\n' + 'Хочешь создать свой мем?'
    bot.send_message(message.chat.id, name_surname + '\n' + start_message, parse_mode='html', reply_markup=markup)


@bot.message_handler(content_types=['text'])
def buttons_logic(message):
    get_message_bot = message.text.strip().lower()
    global flag2, top_text, bottom_text, filename, flag, game
    if get_message_bot == 'да, давай начнем!':
        create_own_meme_buttons(message)
    elif get_message_bot == "задать изображение":
        if game:
            bot.send_message(message.chat.id, 'Жаль, но ладно. Значит продолжим игру в следующий раз',
                             parse_mode='html')
        bot.send_message(message.chat.id,
                         'Чтож, я жду. Теперь следующее присланное фото будет принято для создания', parse_mode='html')
        flag = 1
    elif get_message_bot == "задать верхний текст":
        bot.send_message(message.chat.id, 'Следующее текстовое сообщение станет верхним текстом', parse_mode='html')
        flag2 = 1
    elif get_message_bot == 'посмотреть, что осталось сделать':
        mymes = func.data_checking(top_text, bottom_text, filename)
        bot.send_message(message.chat.id, mymes, parse_mode='html')
    elif get_message_bot == "задать нижний текст":
        bot.send_message(message.chat.id, 'Следующее текстовое сообщение станет нижним текстом', parse_mode='html')
        flag2 = 2
    elif get_message_bot == "сгенерировать":
        if func.data_checking(top_text, bottom_text, filename) == 'У вас все есть, можно приступать к созданию!':
            chat_id = message.chat.id
            dir_adr = os.getcwd()
            img = str(func.launch_it(top_text, bottom_text, filename, dir_adr))
            bot.send_photo(chat_id, photo=open(img, 'rb'))
            os.remove(img)
            flag = 0
            flag2 = 0
            top_text = None
            bottom_text = None
            filename = None
            game = 0
        else:
            bot.send_message(message.chat.id, func.data_checking(top_text, bottom_text, filename), parse_mode='html')
    elif get_message_bot == "рандомная картинка":
        game = 1
        filename = random.randint(0, 50)
        myms = 'Предлагаю мини-игру: от тебя текст, а картинку выберу я. Посмотрим, что получится?' \
               ' Я, кстати, уже выбрал. Твоя очередь'
        bot.send_message(message.chat.id, myms, parse_mode='html')
    elif flag2 == 0:
        bot.send_message(message.chat.id, 'Я не понимаю, напиши /start, чтобы начать', parse_mode='html')
    elif flag2 == 1:
        top_text = func.get_text(message)
        myms = 'Ваш текст: "' + top_text + '" Хотите изменить? Тогда выберите "задать верхний текст" на клавиатуре'
        bot.send_message(message.chat.id, myms, parse_mode='html')
        flag2 = 0
    elif flag2 == 2:
        bottom_text = func.get_text(message)
        myms = 'Ваш текст: "' + bottom_text + '" Хотите изменить? Тогда выберите "задать нижний текст" на клавиатуре'
        bot.send_message(message.chat.id, myms, parse_mode='html')
        flag2 = 0


@bot.message_handler(content_types=['photo'])
def photo(message):
    dir_adr = os.getcwd()
    global flag, filename, game
    if flag:
        if game == 0 and filename is not None:
            os.remove(str(dir_adr) +'/images/' + str(filename) + '.jpg')
        else:
            game = 0
        try:
            file_info = bot.get_file(message.photo[-1].file_id)
            filename = file_info.file_id
            downloaded_file = bot.download_file(file_info.file_path)
            save_adress = str(dir_adr) + '/images/'
            src = save_adress + message.photo[-1].file_id + '.jpg'
            with open(src, 'wb') as new_file:
                new_file.write(downloaded_file)
            bot.reply_to(message, "Фото принято. Если хотите прислать другое фото, выберите снова задать изображение")
        except Exception as e:
            bot.reply_to(message, e)


bot.polling()
