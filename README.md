![уьуьуьуьу](/uploads/4c8139fd14bba83d87a27919dbcf55ba/уьуьуьуьу.png)

# Meme generating telegram bot


It's simple, fast and enjoyable tool for creating memes with your own text and image.  
Or you cant try your fortune and create a random meme    
Bot's name in telegram: @best_meme_generator_bot

## Getting started


If you want to create your bot based on ours, create a token.ini file in the root folder and write your token there. After that you can run the telme.py file

If you want to test our bot, it is available around the clock at the address:
https://msngr.link/tg/best_meme_generator_bot

## Requirements

Python 3.x should be installed on your device

Also you need to install the following libraries:  
- pyTelegramBotAPI  
- Pillow

## Scrincast

Check [the scrincast](https://youtu.be/C3WnE8NV_Fo) for better understanding
